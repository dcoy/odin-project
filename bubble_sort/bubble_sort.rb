def bubble_sort(arr)
  loop do
    swapped = false
    (arr.length - 1).times do |i|
      if arr[i] > arr[i+1]
        arr[i], arr[i+1] = arr[i+1], arr[i]
        swapped = true
      end
    end

    break if not swapped
  end

  arr
end

def bubble_sort_by(arr)
  loop do
    swapped = false
    (arr.length-1).times do |i|
        diff_num = yield(arr[i], arr[i+1])
        if diff_num > 0
          arr[i], arr[i+1] = arr[i+1], arr[i]
        end
    end
    break if not swapped
  end
  arr
end

p bubble_sort([4,3,78,2,0,2])
p bubble_sort_by(["hi","hello","hey"]) { |left,right| left.length - right.length }