# frozen_string_literal: false

dictionary = %w[below down go going horn how howdy it i low own part partner sit]
string = "Howdy partner, sit down! How's it going?"

def substrings(str, dict_array)
  hm = {}
  dict_array.each do |word|
    hm[word] = str.downcase.scan(/#{word}/).count
  end
  p hm
end

substrings(string, dictionary)
